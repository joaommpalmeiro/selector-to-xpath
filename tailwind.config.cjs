/* eslint-env node */

// https://tailwindcss.com/docs/guides/vite#vue
// https://flowbite.com/docs/getting-started/vue/
// https://flowbite-vue.com/pages/getting-started
// https://github.com/tailwindlabs/tailwindcss-forms
// https://tailwindui.com/documentation
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: {
      transitionProperty: {
        // https://tailwindcss.com/docs/transition-property
        zoom: "transform, box-shadow, background-color",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
