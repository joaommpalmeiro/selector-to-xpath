# selector-to-xpath

> https://selector-to-xpath.pages.dev/

![Screenshot](assets/output.png)

Project bootstrapped by [create-vue](https://github.com/vuejs/create-vue).

## Development

- `npm install`
- `npm run dev`
- `npm run lint`
