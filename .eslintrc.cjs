/* eslint-env node */

require("@rushstack/eslint-patch/modern-module-resolution");

// https://eslint.org/docs/latest/user-guide/configuring/language-options#using-configuration-comments
module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-prettier",
  ],
  parserOptions: {
    ecmaVersion: "latest",
  },
};
