# Notes

- https://tailwindcss.com/docs/guides/vite#vue:
  - `npm install -D tailwindcss postcss autoprefixer`+ `npx tailwindcss init -p`
- `npm create vue@3`
- https://github.com/vitejs/vite/tree/main/packages/create-vite/template-vue + https://github.com/vitejs/vite/tree/main/packages/create-vite/template-vue-ts
- https://github.com/sethidden/vue3-eslint-stylelint-demo
- https://github.com/miguelsolorio/vscode-symbols
- `npm i vue && npm i -D @rushstack/eslint-patch @types/node @vitejs/plugin-vue @vue/eslint-config-prettier @vue/eslint-config-typescript @vue/tsconfig eslint eslint-plugin-vue npm-run-all prettier typescript vite vue-tsc`
- https://github.com/antfu/vitesse + https://github.com/antfu/vitesse-lite
- https://www.npmjs.com/package/vue-tabler-icons
- `npm i vue && npm i -D @rushstack/eslint-patch @vitejs/plugin-vue @vue/eslint-config-prettier eslint eslint-plugin-vue prettier vite`
- `npm i -D prettier-plugin-tailwindcss`
- `npm i flowbite flowbite-vue`
- `npm i flowbite`
- `npm i @heroicons/vue`
- `npm i @vueuse/core`

## [Intro to Vue 3](https://www.vuemastery.com/courses/intro-to-vue-3/intro-to-vue3) course by Adam Jahr/Vue Mastery

- https://marketplace.visualstudio.com/items?itemName=Tobermory.es6-string-html
- Repo: https://github.com/Code-Pop/Intro-to-Vue-3
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions
- Double curly brace syntax or mustache syntax.
- Vue is reactive.
- [`v-bind` directive](https://vuejs.org/guide/essentials/class-and-style.html):
  - "Reactive bond".
  - Dynamically bind an attribute to an expression.
  - One-way binding.
  - `<img v-bind:src="image">` or `<img :src="image">` (shorthand).
- [Conditional Rendering](https://vuejs.org/guide/essentials/conditional.html) (e.g., `v-if` directive).
- `v-show` directive is used for toggling an element's visibility instead of adding and removing the element from the DOM entirely like `v-if` does.
- [List Rendering](https://vuejs.org/guide/essentials/list.html) (`v-for` directive):
  - Example: `<div v-for="variant in variants" :key="variant.id">{{ variant.color }}</div>`
  - `v-for="(variant, index) in variants"`
- [Event Handling](https://vuejs.org/guide/essentials/event-handling.html) (`v-on` directive).
  - Example: `<button class="button" v-on:click="addToCart">Add to Cart</button>`
  - `v-on:click` or `@click` (shorthand).
  - Hover: `@mouseover`
- Style binding: `<div :style="{ backgroundColor: variant.color }"></div>`. camelCased property name (e.g., `backgroundColor`) or kebab-cased property name (e.g., `'background-color'`).
- Class binding:
  - `<button :class="{ disabledButton: !inStock }"></button>` (`disabledButton` class).
  - `<div :class="[ isActive ? activeClass : '' ]"></div>` (ternary operators).
- [Computed Properties](https://vuejs.org/guide/essentials/computed.html) (vs. data properties). The benefit of computed properties is that they cache the value.
- Emitting (or bubbling up) events:
  - Telling "parent" when event happens.
  - `addToCart() { this.$em it('add-to-cart') }`
  - `<product-display @add-to-cart="updateCart">`
  - `addToCart() { this.$emit('add-to-cart', this.variants[this.selectedVariant].id) }`
- `v-model` directive:
  - Two-way data binding.
  - Useful for forms.
  - `v-model.number` is a modifier that typecasts the value as a number.
  - `@submit.prevent="onSubmit"` is used to prevent the default behavior (a browser refresh). When this form is submitted, it will trigger the `onSubmit()` method.

### `components/ReviewForm.js` component

```js
app.component("review-form", {
  template: `<form class="review-form" @submit.prevent="onSubmit">
    <h3>Leave a review</h3>
    <label for="name">Name:</label>
    <input id="name" v-model="name">

    <label for="review">Review:</label>
    <textarea id="review" v-model="review"></textarea>

    <label for="rating">Rating:</label>
    <select id="rating" v-model.number="rating">
      <option>5</option>
      <option>4</option>
      <option>3</option>
      <option>2</option>
      <option>1</option>
    </select>

    <input class="button" type="submit" value="Submit">
  </form>`,
  data() {
    return {
      name: "",
      review: "",
      rating: null,
    };
  },
  methods: {
    onSubmit() {
      let productReview = {
        name: this.name,
        review: this.review,
        rating: this.rating,
      };
      this.$emit("review-submitted", productReview);

      // Reset:
      this.name = "";
      this.review = "";
      this.rating = null;
    },
  },
});
```

## [Vue for React Devs: Similarities](https://www.vuemastery.com/courses/vue-for-react-devs-similarities/intro-to-vue-for-react-devs) course by Andy Li/Vue Mastery

- React and Vue are similar:
  - Single-page application (SPA) development
  - With stateful components
  - Powered by VDOM
- They are also very different:
  - They solve the problem in different ways:
    - Vue: full-fledged, framework-style approach
    - React: minimalist, library-style approach
- Repo: https://github.com/Code-Pop/vue-for-react-devs
- https://github.com/vuejs/create-vue
- `ref()`:
  - Vue's version of `useState()`.
  - [Documentation](https://vuejs.org/guide/essentials/template-refs.html#accessing-the-refs).
  - Alternative: `reactive()`. It is intended for object-type data.
- Composition API: Vue's version of React Hooks. Options API: Vue's version of React's class-based API.
- Composables are custom hooks.
- `v-for` and `v-if` can be used together on the same element. The order in which they appear on the same element doesn't matter. `v-if` will always be processed before `v-for`.
- `@click.prevent`: Vue's version of `event.preventDefault()`.
- Class binding can also be used with an array (e.g., `<div :class="[color, bgColor, borderColor]">`).
- `<style scoped></style>`: CSS will be scoped to only the current component and not affect its child components, except the root element of a child component.

## [Vue for React Devs: Differences](https://www.vuemastery.com/courses/vue-for-react-devs-differences/reacting-to-changes) course by Andy Li/Vue Mastery

- Repo: https://github.com/Code-Pop/vue-for-react-devs/tree/Part-2-L9-end
- Vue was implemented with the observer pattern. A state is an object that can be subscribed to. The subscriber will get notified whenever a state change occurs. For example, the component's template is an implicit subscriber of the state in the same component. When a state is changed, the template will re-run itself. `<script>` doesn't need to be run repeatedly (like in React).
- `<template></template>` is an _implicit subscriber_ of the component's state. A computed property is an _explicit subscriber_ (e.g., `const title = computed(() => { return brand.value + ' ' + product.value })`).
- `watch` and `watchEffect`:
  - Be notified of a state change.
  - But not to create a computed property.
  - Use the `watch` function to run a callback whenever a particular state is changed.
  - `watchEffect` is similar to `watch`, but we don't have to specify what to watch. Any subscribable object inside the callback will be subscribed to automatically.
  - Note: `watchEffect` will run the callback once from the start before any state change.
- `const props = defineProps({})`. `defineProps` is a compiler macro (no need to import it). `const emit = defineEmits([])` is used for custom events. `const addToCart = () => { emit('custom-event') }`.
- In Vue, instead of passing callback functions as props like in React, define/emit custom events.
- `v-model` directive: value binding + event binding. Before: `<input :value="text" @input="handleInput" />`. After: `<input v-model="text" />`.
- Default slot (`<slot></slot>`): Vue's version of the children prop. There are also named slots. We can provide a fallback template by putting it inside the `<slot>` element.

### `src/components/ReviewForm.vue` component

```js
const onSubmit = () => {
  // Stringify and parse so that the data being sent is non-reactive:
  const reviewData = JSON.parse(JSON.stringify(review));

  emit("review-submitted", reviewData);

  // Reset:
  Object.assign(review, {
    name: "",
    content: "",
    rating: null,
  });
};

// or

const defaultFormData = {
  name: "",
  content: "",
  rating: null,
};

// Init:
const review = reactive({ ...defaultFormData });

const onSubmit = () => {
  // Reset:
  Object.assign(review, defaultFormData);
};
```

### `src/App.vue` component

- https://vuejs.org/examples/#fetching-data (via `watchEffect`)

```js
const cart = ref([]);

onMounted(() => {
  fetch("http://localhost:3000/cart")
    .then((resp) => resp.json())
    .then((data) => (cart.value = data.content));
});
```
