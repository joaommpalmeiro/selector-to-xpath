import { createApp } from "vue";
import App from "@/App.vue";

// https://github.com/vitejs/vite/blob/main/packages/create-vite/template-vue/src/main.js
// https://flowbite.com/docs/getting-started/vue/
import "@/style.css";
import "flowbite";

createApp(App).mount("#app");
